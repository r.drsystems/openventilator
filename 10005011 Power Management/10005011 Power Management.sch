EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 8
Title ""
Date ""
Rev "1"
Comp ""
Comment1 "Design for OSHPark 4-Layer Service"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 8950 4250 1050 850 
U 5E92767C
F0 "Buck-Boost 24V" 50
F1 "10005011 Buck-Boost 24V.sch" 50
$EndSheet
$Sheet
S 7650 4250 1050 850 
U 5E9C3EF6
F0 "USB HOST" 50
F1 "10005011 USB HOST.sch" 50
$EndSheet
Text Notes 9150 4700 0    118  ~ 0
SHEET7
Text Notes 7800 4700 0    118  ~ 0
SHEET6
$Sheet
S 5050 4250 1050 850 
U 5FB0520C
F0 "Connectors-FAN" 50
F1 "10005011 Connectors-FAN.sch" 50
$EndSheet
Text Notes 5250 4700 0    118  ~ 0
SHEET4
$Sheet
S 3750 4250 1050 850 
U 5FDBA600
F0 "FTDI" 50
F1 "10005011 FTDI.sch" 50
$EndSheet
Text Notes 3950 4700 0    118  ~ 0
SHEET3
$Sheet
S 6350 4250 1050 850 
U 5FEC1BCD
F0 "Const-I-Gen" 50
F1 "10005011 Const-I-Gen.sch" 50
$EndSheet
Text Notes 6550 4700 0    118  ~ 0
SHEET5
$Sheet
S 2400 4250 1050 850 
U 5FEC5422
F0 "Switching-ExtPWR" 50
F1 "10005011 Switching-ExtPWR.sch" 50
$EndSheet
Text Notes 2600 4700 0    118  ~ 0
SHEET2
$Sheet
S 1050 4250 1050 850 
U 5FEC80F9
F0 "PIC-Micro" 50
F1 "10005011 PIC-Micro.sch" 50
$EndSheet
Text Notes 1250 4700 0    118  ~ 0
SHEET1
$Comp
L Mechanical:MountingHole ZM1
U 1 1 5E973F3B
P 2200 3350
F 0 "ZM1" H 2200 3500 50  0000 C CNN
F 1 "MountingHole" H 2300 3305 50  0001 L CNN
F 2 "10005011 Power Management:FIX45_1" H 2200 3350 50  0001 C CNN
F 3 "~" H 2200 3350 50  0001 C CNN
	1    2200 3350
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole ZM2
U 1 1 5E974299
P 1200 3000
F 0 "ZM2" H 1200 3150 50  0000 C CNN
F 1 "MountingHole" H 1300 2955 50  0001 L CNN
F 2 "10005011 Power Management:FIX45_1" H 1200 3000 50  0001 C CNN
F 3 "~" H 1200 3000 50  0001 C CNN
	1    1200 3000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole ZM5
U 1 1 5E974965
P 1350 2550
F 0 "ZM5" H 1350 2700 50  0000 C CNN
F 1 "MountingHole" H 1450 2505 50  0001 L CNN
F 2 "10005011 Power Management:FIX40_1" H 1350 2550 50  0001 C CNN
F 3 "~" H 1350 2550 50  0001 C CNN
	1    1350 2550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole ZM6
U 1 1 5E97496F
P 2200 2550
F 0 "ZM6" H 2200 2700 50  0000 C CNN
F 1 "MountingHole" H 2300 2505 50  0001 L CNN
F 2 "10005011 Power Management:FIX40_1" H 2200 2550 50  0001 C CNN
F 3 "~" H 2200 2550 50  0001 C CNN
	1    2200 2550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole ZM3
U 1 1 5E97529B
P 1350 1350
F 0 "ZM3" H 1350 1500 50  0000 C CNN
F 1 "MountingHole" H 1450 1305 50  0001 L CNN
F 2 "10005011 Power Management:FIX45_1" H 1350 1350 50  0001 C CNN
F 3 "~" H 1350 1350 50  0001 C CNN
	1    1350 1350
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole ZM4
U 1 1 5E9752A5
P 2250 1350
F 0 "ZM4" H 2250 1500 50  0000 C CNN
F 1 "MountingHole" H 2350 1305 50  0001 L CNN
F 2 "10005011 Power Management:FIX45_1" H 2250 1350 50  0001 C CNN
F 3 "~" H 2250 1350 50  0001 C CNN
	1    2250 1350
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole ZM7
U 1 1 5E9752AF
P 2050 1650
F 0 "ZM7" H 2050 1800 50  0000 C CNN
F 1 "MountingHole" H 2150 1605 50  0001 L CNN
F 2 "10005011 Power Management:FIX40_1" H 2050 1650 50  0001 C CNN
F 3 "~" H 2050 1650 50  0001 C CNN
	1    2050 1650
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole ZM8
U 1 1 5E9752B9
P 1550 1650
F 0 "ZM8" H 1550 1800 50  0000 C CNN
F 1 "MountingHole" H 1650 1605 50  0001 L CNN
F 2 "10005011 Power Management:FIX40_1" H 1550 1650 50  0001 C CNN
F 3 "~" H 1550 1650 50  0001 C CNN
	1    1550 1650
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial MIR3
U 1 1 5EA84F61
P 3650 2400
F 0 "MIR3" H 3735 2446 50  0000 L CNN
F 1 "Pad" H 3735 2355 50  0000 L CNN
F 2 "10005011 Power Management:MIRECMS_D-1.5_1" H 3850 2400 50  0001 C CNN
F 3 "~" H 3850 2400 50  0001 C CNN
	1    3650 2400
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial MIR4
U 1 1 5EA8549D
P 3650 2900
F 0 "MIR4" H 3735 2946 50  0000 L CNN
F 1 "Pad" H 3735 2855 50  0000 L CNN
F 2 "10005011 Power Management:MIRECMS_D-1.5_1" H 3850 2900 50  0001 C CNN
F 3 "~" H 3850 2900 50  0001 C CNN
	1    3650 2900
	1    0    0    -1  
$EndComp
Wire Notes Line
	1100 1050 2500 1050
Wire Notes Line
	2500 1050 2500 3700
Wire Notes Line
	2500 3700 1100 3700
Wire Notes Line
	1100 3700 1100 1050
Wire Notes Line
	1450 2650 2100 2650
Wire Notes Line
	2100 2650 2100 3200
Wire Notes Line
	2100 3200 1450 3200
Wire Notes Line
	1450 3200 1450 2650
$Comp
L Mechanical:Fiducial MIR2
U 1 1 5EA84D93
P 3650 1900
F 0 "MIR2" H 3735 1946 50  0000 L CNN
F 1 "Pad" H 3735 1855 50  0000 L CNN
F 2 "10005011 Power Management:MIRECMS_D-1.5_1" H 3850 1900 50  0001 C CNN
F 3 "~" H 3850 1900 50  0001 C CNN
	1    3650 1900
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial MIR1
U 1 1 5EA84054
P 3650 1400
F 0 "MIR1" H 3735 1446 50  0000 L CNN
F 1 "Pad" H 3735 1355 50  0000 L CNN
F 2 "10005011 Power Management:MIRECMS_D-1.5_1" H 3850 1400 50  0001 C CNN
F 3 "~" H 3850 1400 50  0001 C CNN
	1    3650 1400
	1    0    0    -1  
$EndComp
Text Notes 1150 950  0    118  ~ 0
Mounting Holes
Text Notes 3300 950  0    118  ~ 0
Fiducials
$EndSCHEMATC
